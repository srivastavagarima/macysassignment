//
//  ProductDetailViewController.m
//  MacysAssignment
//
//  Created by Garima on 3/15/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "UpdateViewController.h"
#import "Product.h"
#import "ProductDatabase.h"

@interface ProductDetailViewController () {
    Product *info;
}

@end

@implementation ProductDetailViewController

@synthesize productID = _productID;
@synthesize productInfos = _productInfos;

@synthesize productLabel = _productLabel;
@synthesize regPriceLabel = _regPriceLabel;
@synthesize salePriceLabel = _salePriceLabel;
@synthesize colorTextView = _colorTextView;
@synthesize storeTextView = _storeTextView;
@synthesize productImageView = _productImageView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"Product Detail";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)showZoomedImage:(id)sender {
}

- (void) viewWillAppear:(BOOL)animated {
    
    self.productInfos = [ProductDatabase database].product;
    
    info = [_productInfos objectAtIndex:[_productID integerValue]];
      
    _productLabel.text = [@"Item: " stringByAppendingString: info.name];
    _regPriceLabel.text = [@"Reg.: $" stringByAppendingString: info.regularPrice];
    _salePriceLabel.text = [@"Sale: $" stringByAppendingString: info.salePrice ];
    _colorTextView.text = info.color;//[[info.color componentsSeparatedByString:@" "] componentsJoinedByString:@","];
    _storeTextView.text = info.store;//[[info.store componentsSeparatedByString:@" "] componentsJoinedByString:@","];
    _productDescTextView.text = info.description;
    UIImage *img = [UIImage imageNamed:info.image];
    [_productImageView setImage:img];

}

- (IBAction)update:(id)sender {
    
    UpdateViewController *update = [[UpdateViewController alloc] initWithNibName:@"UpdateViewController" bundle:nil];
    
    update.productName = info.name;
    update.regPrice = info.regularPrice;
    update.salePrice = info.salePrice;
    update.productImage = info.image;
    update.colorList = _colorTextView.text;
    update.storeList = _storeTextView.text;
    update.productID = [NSString stringWithFormat:@"%d", info.uniqueId];
    update.productDesc = info.description;
    [self.navigationController pushViewController:update animated:YES];
    update = nil;
}

@end
