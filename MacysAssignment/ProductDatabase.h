//
//  ProductDatabase.h
//  MacysAssignment
//
//  Created by Garima on 3/13/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ProductDatabase : NSObject {
    sqlite3 *_database;
    NSString *databasePath;
}


+ (ProductDatabase*)database;
- (NSArray *)product;

- (BOOL) saveData:(NSString*)productId prodName:(NSString*)productName desc:(NSString*)description
        regPrice:(NSString*)regPrice salePrice:(NSString*)salePrice
            image:(NSString*) image color:(NSString*) color store:(NSString*) store;

- (BOOL) deleteData:(NSString*) productId;

@end
