//
//  main.m
//  MacysAssignment
//
//  Created by Garima on 3/12/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MacysAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MacysAppDelegate class]));
    }
}
