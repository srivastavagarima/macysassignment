//
//  ProductDetailViewController.h
//  MacysAssignment
//
//  Created by Garima on 3/15/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailViewController : UIViewController

@property (nonatomic, strong) NSString *productID;
@property (nonatomic, strong) NSArray *productInfos;

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UILabel *regPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *salePriceLabel;
@property (weak, nonatomic) IBOutlet UITextView *storeTextView;
@property (weak, nonatomic) IBOutlet UITextView *colorTextView;
@property (weak, nonatomic) IBOutlet UITextView *productDescTextView;

@end
