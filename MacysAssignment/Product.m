//
//  Product.m
//  MacysAssignment
//
//  Created by Garima on 3/12/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize uniqueId = _uniqueId;
@synthesize name = _name;
@synthesize description = _description;
@synthesize regularPrice = _regularPrice;
@synthesize salePrice = _salePrice;
@synthesize image = _image;
@synthesize color = _color;
@synthesize store = _store;

- (id)initWithUniqueId:(int)uniqueId name:(NSString *)name description:(NSString *)description
          regularPrice:(NSString *)regularPrice salePrice:(NSString *)salePrice
                 image:(NSString*)image color:(NSString*) color store: (NSString*)store {
    
    self = [super init];
    
    if(self) {
        self.uniqueId = uniqueId;
        self.name = name;
        self.description = description;
        self.regularPrice = regularPrice;
        self.salePrice = salePrice;
        self.image = image;
        self.color = color;
        self.store = store;
    }
    
    return self;
}

@end
