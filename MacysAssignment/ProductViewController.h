//
//  ProductViewController.h
//  MacysAssignment
//
//  Created by Garima on 3/15/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, weak) IBOutlet UITableView *productTableView;
@property (nonatomic, strong) NSMutableArray *productInfos;
@property (nonatomic, assign) NSInteger productCount;
@property (nonatomic, strong) NSString *insertSuccess;
@end
