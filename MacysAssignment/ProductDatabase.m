//
//  ProductDatabase.m
//  MacysAssignment
//
//  Created by Garima on 3/13/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "ProductDatabase.h"
#import "Product.h"

@implementation ProductDatabase

static ProductDatabase *_database;

+ (ProductDatabase*)database {
    if (_database == nil) {
        _database = [[ProductDatabase alloc] init];
    }
    return _database;
}

- (id)init {
    
    if ((self = [super init])) {
        
        NSError *error;
        
        // Get the documents directory
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *docsDir = dirPaths[0];
        
        // Build the path to the database file
        databasePath = [docsDir stringByAppendingPathComponent: @"productlist.sqlite3"];
        
        
        NSString *sqLiteDb = [[NSBundle mainBundle] pathForResource:@"productlist"
                                                             ofType:@"sqlite3"];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath: databasePath ] == NO)
            [[NSFileManager defaultManager] copyItemAtPath:sqLiteDb toPath:databasePath error:&error];
        
        if (sqlite3_open([databasePath UTF8String], &_database) != SQLITE_OK) {
            NSLog(@"Failed to open database!");
        }
    }
    return self;
}

- (NSArray *)product {
    
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT id, name, description, regular_price, sale_price, image, color, store FROM products ORDER BY id ASC";
    sqlite3_stmt *statement;
    if (sqlite3_open([databasePath UTF8String], &_database) == SQLITE_OK)
    {
        if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil)
            == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                int uniqueId = sqlite3_column_int(statement, 0);
                char *nameChars = (char *) sqlite3_column_text(statement, 1);
                char *descChars = (char *) sqlite3_column_text(statement, 2);
                char *regPChars = (char *) sqlite3_column_text(statement, 3);
                char *salePChars = (char *) sqlite3_column_text(statement, 4);
                char *imageChars = (char *) sqlite3_column_text(statement, 5);
                char *colorChars = (char *) sqlite3_column_text(statement, 6);
                char *storeChars = (char *) sqlite3_column_text(statement, 7);
                
                NSString *name = [[NSString alloc] initWithUTF8String:nameChars];
                NSString *description = [[NSString alloc] initWithUTF8String:descChars];
                NSString *regularPrice = [[NSString alloc] initWithUTF8String:regPChars];
                NSString *salePrice = [[NSString alloc] initWithUTF8String:salePChars];
                NSString *image = [[NSString alloc] initWithUTF8String:imageChars];
                NSString *color = [[NSString alloc] initWithUTF8String:colorChars];
                NSString *store = [[NSString alloc] initWithUTF8String:storeChars];
                
                Product *info = [[Product alloc]
                                 initWithUniqueId:uniqueId name:name description:description
                                 regularPrice:regularPrice salePrice:salePrice image:image color:color store:store];
                [retval addObject:info];
            }
            sqlite3_finalize(statement);
            sqlite3_close(_database);
        }
        
    }
    return retval;
    
}

-(BOOL) saveData:(NSString*)productId prodName:(NSString*)productName desc:(NSString*)description
        regPrice:(NSString*)regPrice salePrice:(NSString*)salePrice
           image:(NSString*)image color:(NSString*) color store:(NSString*) store {
    
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    const char *dbpath = [databasePath UTF8String];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        if (sqlite3_open(dbpath, &_database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt ="create table if not exists products (id integer primary key autoincrement, name text, description text, regular_price text, sale_price text,image text, color text, store text)";
            if (sqlite3_exec(_database, sql_stmt, NULL, NULL, &errMsg)!= SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            sqlite3_close(_database);
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    
    sqlite3_stmt *statement;
    int result;
    if (sqlite3_open_v2(dbpath, &_database, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK)
    {
        
        if ([productId intValue] >= 0) { //Update
            NSString *updateSQL = [NSString stringWithFormat:@"UPDATE products SET name = ?, description = ?, regular_price = ?, sale_price = ?, image = ?, color = ?, store = ? where id = ?"];
            const char *update_stmt = [updateSQL UTF8String];
            result = sqlite3_prepare_v2(_database, update_stmt,-1, &statement, NULL);
            if (result == SQLITE_OK) {
                sqlite3_bind_int(statement, 8, [productId intValue]);
                sqlite3_bind_text(statement, 1, [productName UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(statement, 2, [description UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(statement, 3, [regPrice UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(statement, 4, [salePrice UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(statement, 5, [image UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(statement, 6, [color UTF8String], -1, SQLITE_TRANSIENT);
                sqlite3_bind_text(statement, 7, [store UTF8String], -1, SQLITE_TRANSIENT);
            }
            
            if (sqlite3_step(statement) == SQLITE_DONE) {
                isSuccess = YES;
            }
            else {
                isSuccess = NO;
            }
        } else { //Insert
            NSString *insertSQL = [NSString stringWithFormat:
                                   @"INSERT INTO products (name, description, regular_price, sale_price, image, color, store) VALUES (\"%@\", \"%@\", \"%@\",\"%@\", \"%@\", \"%@\", \"%@\")",
                                   productName,
                                   description,
                                   regPrice, salePrice, image, color, store];
            
            const char *insert_stmt = [insertSQL UTF8String];
            result = sqlite3_prepare_v2(_database, insert_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                isSuccess = YES;
            }
        }
        
        sqlite3_reset(statement);
        sqlite3_finalize(statement);
        sqlite3_close(_database);
    }
    
    return isSuccess;
    
}

- (BOOL) deleteData:(NSString *)productId {
    
    BOOL isSuccess = YES;
    
    sqlite3_stmt *statement;
    
    if (sqlite3_open_v2([databasePath UTF8String], &_database, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK)
    {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE from products where id = ?"];
        const char *delete_stmt = [deleteSQL UTF8String];
        int result = sqlite3_prepare_v2(_database, delete_stmt,-1, &statement, NULL);
        
        if (result == SQLITE_OK) {
            result = sqlite3_bind_int(statement, 1, [productId intValue]);
        }
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            isSuccess = YES;
        }
        else {
            isSuccess = NO;
        }
        sqlite3_finalize(statement);
        sqlite3_close(_database);
    }
    
    return isSuccess;
    
}

@end
