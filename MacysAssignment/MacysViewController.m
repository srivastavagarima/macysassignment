//
//  MacysViewController.m
//  MacysAssignment
//
//  Created by Garima on 3/12/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "MacysViewController.h"
#import "ProductViewController.h"

@interface MacysViewController ()

@end

@implementation MacysViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.title = @"Macy's Assignment";
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)welcomeButton:(id)sender {
    
    ProductViewController *prodViewController = [[ProductViewController alloc] initWithNibName:@"ProductView" bundle:nil];
    [self.navigationController pushViewController:prodViewController animated:YES];
}


@end
