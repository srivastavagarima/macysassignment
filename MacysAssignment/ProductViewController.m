//
//  ProductViewController.m
//  MacysAssignment
//
//  Created by Garima on 3/15/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "ProductViewController.h"
#import "Product.h"
#import "ProductDatabase.h"
#import "ProductDetailViewController.h"
#import "UpdateViewController.h"

@interface ProductViewController ()

@end

@implementation ProductViewController
@synthesize productInfos = _productInfos;
@synthesize productCount = _productCount;
@synthesize productTableView = _productTableView;
@synthesize insertSuccess = _insertSuccess;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"Products";
    
    _productCount = 0;
}

- (void) viewWillAppear:(BOOL)animated {

    self.productInfos = [[ProductDatabase database].product mutableCopy];
    [self.productTableView reloadData];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_productCount == 0)
        return _productCount;
    else
        return [self.productInfos count];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell =
    [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
    }
    
    // Set up the cell...
    Product *info = [_productInfos objectAtIndex:indexPath.row];
    cell.textLabel.text = info.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"$%@, $%@",
                                 info.regularPrice, info.salePrice];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProductDetailViewController *productDetail = [[ProductDetailViewController alloc] initWithNibName:@"ProductDetailViewController" bundle:nil];
 //   Product *info = [_productInfos objectAtIndex:indexPath.row];

    productDetail.productID = [NSString stringWithFormat:@"%d",indexPath.row];
    
    [self.navigationController pushViewController:productDetail animated:YES];
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 50, 50)];
    label1.backgroundColor = [UIColor lightGrayColor];
    label1.textAlignment = NSTextAlignmentLeft;
    label1.font = [UIFont boldSystemFontOfSize:14.0f];
    label1.text = @"Products";
    return label1;
}

- (IBAction)createProduct:(id)sender {
    
    UpdateViewController *update = [[UpdateViewController alloc] initWithNibName:@"UpdateViewController" bundle:nil];
    
    update.productName = @"";
    update.regPrice = @"";
    update.salePrice = @"";
    update.productImage = @"abcd.png";
    update.colorList = @"";
    update.storeList = @"";
    update.productID = @"-1";
    update.productDesc = @"";
    
    [self.navigationController pushViewController:update animated:YES];
    
}

- (IBAction)showProduct:(id)sender {

    _productCount = [self.productInfos count];
    [self.productTableView reloadData];

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	
	if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        Product *info = [_productInfos objectAtIndex:indexPath.row];
        
        BOOL isSuccess = [[ProductDatabase database] deleteData: [NSString stringWithFormat:@"%d", info.uniqueId]];
        
        if (isSuccess) {
            _productCount--;
            [self.productInfos removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationRight];
        }
        
        [tableView reloadData];
	}
}

@end
