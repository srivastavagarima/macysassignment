//
//  UpdateViewController.m
//  MacysAssignment
//
//  Created by Garima on 3/16/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "UpdateViewController.h"
#import "ProductDetailViewController.h"
#import "ProductViewController.h"
#import "ProductDatabase.h"

@interface UpdateViewController () {
    
    CGRect keyboardBounds;
}

@end

@implementation UpdateViewController

@synthesize productName = _productName;
@synthesize productDesc = _productDesc;
@synthesize productImage = _productImage;
@synthesize regPrice = _regPrice;
@synthesize salePrice = _salePrice;
@synthesize colorList = _colorList;
@synthesize storeList = _storeList;
@synthesize productID = _productID;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    prodNameTextView.text = _productName;
    prodescriptionTextView.text = _productDesc;
    regPriceTextView.text = _regPrice;
    salePriceTextView.text = _salePrice;
    colorTextView.text = _colorList;
    storeTextView.text = _storeList;
    
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)saveData:(id)sender {
    
    [prodNameTextView resignFirstResponder];
    [prodescriptionTextView resignFirstResponder];
    [regPriceTextView resignFirstResponder];
    [salePriceTextView resignFirstResponder];
    [colorTextView resignFirstResponder];
    [storeTextView resignFirstResponder];
    
    BOOL success = NO;
    NSString *alertString = @"Data Update failed";
    if (prodNameTextView.text.length > 0 && prodescriptionTextView.text.length > 0 &&
        regPriceTextView.text.length > 0 && salePriceTextView.text.length>0 && colorTextView.text.length > 0
        && storeTextView.text.length > 0)
    {
        success = [[ProductDatabase database] saveData:_productID prodName:prodNameTextView.text desc: prodescriptionTextView.text
                                              regPrice:regPriceTextView.text salePrice:salePriceTextView.text
                                                 image:_productImage color:colorTextView.text store:storeTextView.text];
    }
    else {
        alertString = @"Enter all fields";
    }
    if (success == NO) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertString message:nil
                                                      delegate:nil cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
        [alert show];
    }
    
    [self scrollViewToOrigin];
    [sender setEnabled:NO];
}

- (void) scrollViewToOrigin {
    
    CGSize csize = scrollView.contentSize;
    csize.height = self.view.frame.size.height;//storeTextView.frame.origin.y + storeTextView.frame.size.height;
    scrollView.contentSize = csize;
	[scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

#pragma mark - Text field delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {

    
    [self scrollViewToCenterOfScreen:textField];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
   // [scrollView setFrame:CGRectMake(10, 50, 300, 350)];
    [self scrollViewToOrigin];
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self scrollViewToCenterOfScreen:textView];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (void)scrollViewToCenterOfScreen:(UIView *)theView {
    
	CGFloat viewCenterY = theView.center.y;
	CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    
	CGFloat availableHeight = applicationFrame.size.height - keyboardBounds.size.height;	// Remove area covered by keyboard
    
	CGFloat y = viewCenterY - availableHeight / 2.0;
	if (y < 0) {
		y = 0;
	}
    
	scrollView.contentSize = CGSizeMake(applicationFrame.size.width, applicationFrame.size.height + keyboardBounds.size.height);
	[scrollView setContentOffset:CGPointMake(0, y-25) animated:YES];
}


- (void)keyboardNotification:(NSNotification*)notification {
	NSDictionary *userInfo = [notification userInfo];
	NSValue *keyboardBoundsValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
	[keyboardBoundsValue getValue:&keyboardBounds];
}

- (void) viewWillDisappear:(BOOL)animated {
    
    ProductDetailViewController *detail = [[ProductDetailViewController alloc] init];
    detail.productID = _productID;
    detail = nil;
    
    ProductViewController *view = [[ProductViewController alloc] init];
    view.insertSuccess = @"DONE";
    view = nil;
}

@end
