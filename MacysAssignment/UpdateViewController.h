//
//  UpdateViewController.h
//  MacysAssignment
//
//  Created by Garima on 3/16/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UpdateViewController : UIViewController <UITextFieldDelegate, UITextViewDelegate> {
    
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UITextField *prodNameTextView;
    __weak IBOutlet UITextField *prodescriptionTextView;
    __weak IBOutlet UITextField *regPriceTextView;
    __weak IBOutlet UITextField *salePriceTextView;

    __weak IBOutlet UITextField *colorTextView;
    __weak IBOutlet UITextField *storeTextView;
}

@property (nonatomic, strong) NSString *productID;
@property (nonatomic, strong) NSString *productName;
@property (nonatomic, strong) NSString *productDesc;
@property (nonatomic, strong) NSString *productImage;
@property (nonatomic, strong) NSString *regPrice;
@property (nonatomic, strong) NSString *salePrice;
@property (nonatomic, strong) NSString *storeList;
@property (nonatomic, strong) NSString *colorList;

-(IBAction)saveData:(id)sender;
@end
