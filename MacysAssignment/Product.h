//
//  Product.h
//  MacysAssignment
//
//  Created by Garima on 3/12/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject


@property (nonatomic, assign) int uniqueId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *description;
@property (nonatomic, copy) NSString *regularPrice;
@property (nonatomic, copy) NSString *salePrice;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *color;
@property (nonatomic, copy) NSString *store;


- (id)initWithUniqueId:(int)uniqueId name:(NSString *)name description:(NSString *)description
          regularPrice:(NSString *)regularPrice salePrice:(NSString *)salePrice image:(NSString*)
           image color:(NSString*) color store: (NSString*)store;
@end
